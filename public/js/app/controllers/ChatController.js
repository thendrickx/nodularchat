app.controller("ChatCtrl",["$scope", function($scope){
    $scope.messageToSend = "";
    $scope.userName="Guest";
    $scope.messages = [{"author":"ChatRoom", "text":"Welcome"}];

    var socket = io.connect();

    socket.on("message", function(msg){
       $scope.$apply(function(){$scope.messages.push(msg)});
    });

    $scope.sendMessage = function(msg){
        if(msg && msg.length > 0) {
            var messageObject = {"author":$scope.userName,"text":msg};
            $scope.messages.push(messageObject);
            $scope.messageToSend = "";

            socket.emit("message",messageObject);
        }
    }

    $scope.getRoom = function(query) {
        return ["aa","bb","cc","dd","ee"];
    }
}]);